FROM python:3.9.0
CMD mkdir -p /app
WORKDIR /app
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt
ENV PORT= 

COPY . .
CMD streamlit run app.py --server.port=${PORT}  --browser.serverAddress="0.0.0.0"