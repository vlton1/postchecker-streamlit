import requests
import streamlit as st
from annotated_text import annotated_text
import json
from transformers import pipeline
import operator
# import shap
import tweepy
import pandas as pd

hide_streamlit_style = """
<style>
# MainMenu {visibility: hidden;}
footer {visibility: hidden;}
header {visibility: hidden;}
.css-18e3th9 {
                    padding-top: 0rem;
                    padding-bottom: 10rem;
                    padding-left: 5rem;
                    padding-right: 5rem;
                }
               .css-1d391kg {
                    padding-top: 3.5rem;
                    padding-right: 1rem;
                    padding-bottom: 3.5rem;
                    padding-left: 1rem;
                }

</style>

"""
st.markdown(hide_streamlit_style, unsafe_allow_html=True)


@st.experimental_memo
def twitter_auth():

    # Credentials
    api_key = "ixtorHVAWeF458VunIy20WwME"
    api_secret = "CSEUFOZEJB0yVUAS2fIjN7GbbRXZTCAGMBG8cDzrRurofQxI5f"
    access_token = "1567067940213911552-Bijv8sJ9DGgRNxiHjR1mb9vo3rAbYw"
    access_token_secret = "HWcO2BdMDisb6ABmHE4hU0ys09blqsGk43DCkFlsUtGiI"

    # Gainaing access and connecting to Twitter API using Credentials
    auth = tweepy.OAuth1UserHandler(
        api_key, api_secret, access_token, access_token_secret)
    api = tweepy.API(auth)
    return api


def ner(message):
    method = "post"
    url = "https://twitternerapi2.azurewebsites.net/extract"
    headers = {'Content-type': 'application/json',
               'x-api-key': 'api_token'}
    payload = {'text': message}
    response = requests.request(method, url, json=payload, headers=headers)
    return response


def fake_detection(message):
    method = "post"
    url = "https://fakenews-6itaj22f6q-uc.a.run.app/detect_fake"
    headers = {'Content-type': 'application/json',
               'x-api-key': 'api_token'}
    payload = {'text': message}
    response = requests.request(method, url, json=payload, headers=headers)
    return response


def sentiment(message):
    method = "post"
    url = "http://20.167.25.143/sentiment"
    headers = {'Content-type': 'application/json',
               'x-api-key': 'api_token'}
    payload = {'text': message}
    response = requests.request(method, url, json=payload, headers=headers)
    return response


def get_tuple(ner_output):
    token_lst = []
    for token in ner_output:
        token_lst.append(token['word'].strip())
    a_tuple = tuple(token_lst)
    return a_tuple


def tuple_create(token):
    if token['entity_group'] == 'person':
        a_tuple = (token['word'], token['entity_group'], '#b4a7d6')

    elif token['entity_group'] == 'product':
        a_tuple = (token['word'], token['entity_group'], '#9fc5e8')

    elif token['entity_group'] == 'location':
        a_tuple = (token['word'], token['entity_group'], '#b6d7a8')

    elif token['entity_group'] == 'group':
        a_tuple = (token['word'], token['entity_group'], '#f5ea6e')

    elif token['entity_group'] == 'corporation':
        a_tuple = (token['word'], token['entity_group'], '#e5aee8')

    elif token['entity_group'] == 'event':
        a_tuple = (token['word'], token['entity_group'], '#96e0df')

    else:
        a_tuple = (token['word'], token['entity_group'], '#f9cb9c')
    return a_tuple


def keyword(text, ner_output):
    token_lst = []
    i = 0
    for token in ner_output:
        if i == token['start']:
            a_tuple = tuple_create(token)
            token_lst.append(a_tuple)
            i = token['end'] + 1
        else:
            token_lst.append(text[i: token['start']])
            a_tuple = tuple_create(token)
            token_lst.append(a_tuple)
            i = token['end'] + 1
    if i < len(text):
        token_lst.append(text[i:])
    return token_lst


st.subheader("Analyze the twitter post to get more information")
st.markdown("Instructions" + "\n" + "1. Copy a twitter post "+"\n" +
            "2. Paste the tweet below" + "\n" + "3. Wait for analyzed result")
message = st.text_area("Copy and paste the twitter post here", key='input')
st.caption('Click on **Analyze** button to start analyzing the post.')
a_tokens = ''
a_tuple = ()
option = ''
api = twitter_auth()
m = st.markdown("""
<style>
div.stButton > button:hover{
    border-color: #1DA1F2;
    color: #1DA1F2;
}
div.stButton > button:active {
	border-color: #1DA1F2;
    color: #1DA1F2;
}
</style>""", unsafe_allow_html=True)
if st.button("Analyze"):
    if message == '':
        st.error('Please enter text')
    else:
        with st.spinner('Analyzing text. Please Wait...'):
            st.session_state = {}
            st.session_state['message'] = message
            output_ner = ner(message)
            output_sent = sentiment(message)
            output_fake = fake_detection(message)
            results = json.loads(output_ner.text)['data']
            results_sent = json.loads(output_sent.text)['data']
            # results_spam = spam(message)
            result_fake = json.loads(output_fake.text)['data']['output']
            ner_output = results['output']
            text = results['text']
            a_tokens = keyword(text, ner_output)
            a_tuple = get_tuple(ner_output)
            st.session_state['key'] = a_tuple
            st.session_state['tokens'] = a_tokens
            sent_score = results_sent['sentiment_score']
            st.session_state['result_fake'] = result_fake
            sent_score = dict(
                sorted(sent_score.items(), key=operator.itemgetter(1), reverse=True))
            first_value = next(iter(sent_score))
            st.session_state['sent_score'] = sent_score
            st.session_state['first_value'] = first_value
            st.success('Done!')


st.markdown("""<hr style="height:1px;border:none;color:#333;background-color:#1DA1F2;" /> """,
            unsafe_allow_html=True)

mystyle = '''
    <style>
        p {
            text-align: justify;
        }
    </style>
    '''

st.markdown(mystyle, unsafe_allow_html=True)
st.subheader("Fake News Detection")

c1, c2 = st.columns((1.7, 3))


with c1:
    st.info(
        "Fake news detection system help to identify fake tweets on Twitter. This system will tell you the probablity of the tweet being fake.")
with c2:
    if 'tokens' not in st.session_state:
        st.markdown('')
    else:
        score = str(
            int(round(st.session_state['result_fake']['Fake'], 2) * 100))
        st.subheader(score + '%')
        if int(round(st.session_state['result_fake']['Fake'], 2) * 100) > 80:
            st.markdown('Warning! There is a ' + score + '%' +
                        ' chance that this twitter post is **FAKE**')
        else:
            st.markdown('There is a ' + score + '%' +
                        ' chance that this twitter post is **FAKE**')
st.warning("**Please note that this detector is not always accurate due to the constant changes on Twitter**." + "\n" +
           "We recommend that you use the **Keyword Extractor** and **Keyword Search** provided below to get a better idea of the current updates on Twitter.", icon="⚠️")

st.markdown("""<hr style="height:1px;border:none;color:#333;background-color:#1DA1F2;" /> """,
            unsafe_allow_html=True)
st.subheader("Keyword Extractor")

c1, c2 = st.columns((1.7, 3))
with c1:

    st.info("You can know a lot about your text data by only a few keywords. These keywords will help you to determine whether you can trust this tweet.")

with c2:
    if 'tokens' not in st.session_state:
        st.markdown('')
    else:
        annotated_text(*st.session_state['tokens'])


st.markdown("""<hr style="height:1px;border:none;color:#333;background-color:#1DA1F2;" /> """,
            unsafe_allow_html=True)

st.subheader("Sentiment Analysis")

# Generate Three equal columns
c1, c2 = st.columns((1.7, 3))

neg_sentiment = '<p style="font-family:sans-serif; color:#E74C3C; font-size: 35px;">Negative</p>'
neutral_sentiment = '<p style="font-family:sans-serif; color:#f1c232; font-size: 35px;">Neutral</p>'
pos_sentiment = '<p style="font-family:sans-serif; color:#38761d; font-size: 35px;">Positive</p>'

with c1:
    st.info(
        "Sentiment Analysis is a way of finding out the strength of opinion that is expressed by the author of the post.")
with c2:
    if 'tokens' not in st.session_state:
        st.markdown('')
    else:
        if st.session_state['first_value'] == 'Negative':
            score = str(int(
                round(st.session_state['sent_score'][st.session_state['first_value']], 2) * 100))
            st.markdown(neg_sentiment, unsafe_allow_html=True)
            st.markdown('This tweet has a ' +
                        score + '%' + ' chance that the author has a negative opinion.')

        elif st.session_state['first_value'] == 'Neutral':
            score = str(int(
                round(st.session_state['sent_score'][st.session_state['first_value']], 2) * 100))
            st.markdown(neutral_sentiment, unsafe_allow_html=True)
            st.write('This tweet has a ' + score + '%' +
                     ' chance that the author has a neutral perspective.')

        elif st.session_state['first_value'] == 'Positive':
            score = str(int(
                round(st.session_state['sent_score'][st.session_state['first_value']], 2) * 100))
            st.markdown(pos_sentiment, unsafe_allow_html=True)
            st.write('This tweet has a ' + score + '%' +
                     ' chance that the author has a positive opinion.')


st.markdown("""<hr style="height:1px;border:none;color:#333;background-color:#1DA1F2;" /> """,
            unsafe_allow_html=True)

c1, c2 = st.columns((1.7, 3))
with c1:
    st.markdown("<h2 style='text-align: center; color: black;'>Keyword Search</h2>",
                unsafe_allow_html=True)
    # st.subheader("Keyword Search")

with c2:
    st.info("Keyword search allows you to search for different keywords on twitter in real time. We recommend that you search for the keywords extracted from the post to get the current Top 5 most popular related tweets.")

if 'key' in st.session_state:
    option = st.selectbox('Choose a keyword', st.session_state['key'])
    if st.session_state['key'] != ():
        tweets = tweepy.Cursor(api.search_tweets,
                               q=option, lang="en",
                               tweet_mode='extended', result_type="popular").items(5)

        list_tweets = [tweet for tweet in tweets]
        df_tweet_lst = []
        for tweet in list_tweets:
            dictionary = {}
            username = tweet.user.screen_name
            followers = tweet.user.followers_count
            totaltweets = tweet.user.statuses_count
            retweetcount = tweet.retweet_count
            text = tweet.full_text
            likes = tweet.favorite_count
            dictionary['Username'] = tweet.user.screen_name
            dictionary['Tweet'] = tweet.full_text
            dictionary['Followers'] = tweet.user.followers_count
            dictionary['Likes'] = tweet.favorite_count
            df_tweet_lst.append(dictionary)
        df = pd.DataFrame(df_tweet_lst)

        hide_table_row_index = """
            <style>
            thead tr th:first-child {display:none}
            tbody th {display:none}
            </style>
            """

        # Inject CSS with Markdown
        st.markdown(hide_table_row_index, unsafe_allow_html=True)

        st.table(
            df)
    else:
        st.write('No keywords were extraced from post')

else:
    option = st.selectbox('Choose a keyword', (''))
